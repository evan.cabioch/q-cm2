﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace Q_CM2
{ 
    public class Serie
    {
        protected int matiere;
        protected bool commencee;
        protected bool terminee;
        protected Question[] questions;
       
        public Serie(int matiere, bool commencee, bool terminee, Question[] questions)
        {
            this.matiere = matiere;
            this.commencee = commencee;
            this.terminee = terminee;
            this.questions = questions;
        }

        public int Matiere
        {
            get { return matiere; }
            set { matiere = value; }
        }

        public bool Commencee
        {
            get { return commencee; }
            set { commencee = value; }
        }

        public bool Terminee
        {
            get { return terminee; }
            set { terminee = value; }
        }

   

        public Question getFirstUnansweredQuestion()
        {
            for(int i = 0; i < questions.Length; i++)
            {
                if (!questions[i].Repondue)
                {
                    return questions[i];
                }
            }

            return null;
        }

    }

    public class Question : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected string enonce;
        protected int indexBonneReponse;
        protected string explication;
        protected bool repondue = false;
        protected bool juste = false;
        protected Reponses reponses = new Reponses();

        public Question(string enonce, string[] reponses, int indexBonneReponse, string explication)
        {
            this.enonce = enonce;
            this.indexBonneReponse = indexBonneReponse;
            this.explication = explication;

            foreach (string reponse in reponses)
            {
                this.reponses.Add(new Reponse(reponse));
            }
        }

        public string Enonce
        {
            get { return enonce; }
            set { enonce = value; OnPropertyChanged("Enonce"); }
        }

        void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public int IndexBonneReponse
        {
            get { return indexBonneReponse; }
            set { indexBonneReponse = value; }
        }

        public string Explication
        {
            get { return explication; }
            set { explication = value; }
        }

        public bool Repondue
        {
            get { return repondue; }
            set { repondue = value; }
        }

        public bool Juste
        {
            get { return juste; }
            set { juste = value; }
        }

        

        public Reponse getReponse(int i)
        {
            return reponses[i];
        }
        
        public void updateQuestion(Question newQuestion)
        {
            this.Enonce = newQuestion.Enonce;
            this.indexBonneReponse = newQuestion.IndexBonneReponse;
            this.explication = newQuestion.Explication;

            for (int i = 0; i < 4; i++)
            {
                this.getReponse(i).Text = newQuestion.getReponse(i).Text;
            }
        }
       
    }

    public class Reponses : ObservableCollection<Reponse>
    {
        
    }

    public class Reponse : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected string _text;

        public Reponse(string text)
        {
            _text = text;
        }
        public string Text
        {
            get { return _text; }
            set
            {
                _text = value;
                OnPropertyChanged("Text");
            }
        }

        void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public override string ToString()
        {
            return _text;
        }
    }

    class Model
    {
        protected enum matiere { MATH = 0, FRANCAIS, ANGLAIS, SCIENCES, HISTOIRE, GEOGRAPHIE };
        private static Model instance;
        private List<Serie> series;
        private Serie currentSerie;
        private XElement doc;

        private Model()
        {
            this.series = new List<Serie>();

            doc = XElement.Load("questions.xml");
            IEnumerable<XElement> series = doc.Elements("serie");

            foreach (var serie in series)
            {
                IEnumerable<XAttribute> attributes = serie.Attributes();

                string matiere = (from a in attributes where a.Name == "matiere"
                               select (string) a.Value).First();

                Debug.Write("Matière : " + matiere);
                string started = (from a in attributes
                                  where a.Name == "started"
                                  select (string)a.Value).First();
                string completed = (from a in attributes
                                  where a.Name == "completed"
                                  select (string)a.Value).First();

                IEnumerable<XElement> questionsElements = serie.Elements("question");

                Question[] questions = new Question[5];

                int cptQuestions = 0;
               
                foreach(var question in questionsElements)
                {
                    string enonce = question.Element("enonce").Value;
                   string[] reponses = new string[4];
                    IEnumerable<XElement> reponsesElement = question.Elements("reponse");
                       
                    int indexGoodAnswer = 0;
                    bool goodAnswerFound = false;
                    int cptReponses = 0;
                    foreach(var reponse in reponsesElement)
                    {
                        bool goodAnswer = bool.Parse((from a in reponse.Attributes()
                                             where a.Name == "good-answer" 
                                             select a.Value).First());

                        if (!goodAnswer && !goodAnswerFound)
                        {
                            indexGoodAnswer++;
                        }
                        else
                        {
                            goodAnswerFound = true;
                        }

                        reponses[cptReponses] = reponse.Value;
                        cptReponses++;
                    }

                    bool done = bool.Parse((from a in question.Attributes()
                                            where a.Name == "done"
                                            select (string)a.Value).First());
                    bool right = bool.Parse((from a in question.Attributes()
                                            where a.Name == "right"
                                            select (string)a.Value).First());

                    string explication = question.Element("explication").Value;

                    questions[cptQuestions] = new Question(enonce, reponses, indexGoodAnswer, explication);
                    cptQuestions++;
                }

                this.series.Add(new Serie(int.Parse(matiere), bool.Parse(started), bool.Parse(completed), questions));
            }
        }

        public static Model getInstance()
        {
            if (instance == null)
            {
                instance = new Model();

            }
            return instance;
        }

        public  Serie getFirstSerie(int matiere)
        {
            foreach(Serie serie in series)
            {
                if(serie.Matiere == matiere && !serie.Terminee)
                {
                    if(!serie.Commencee)
                    {
                        serie.Commencee = true;
                        this.currentSerie = serie;
                    }

                    return serie;
                }
            }

            return null;
        }

        public int questionAnswered(int matiere, bool juste)
        {
            

            Question questionRepondue = getFirstSerie(matiere).getFirstUnansweredQuestion();
            questionRepondue.Repondue = true;
            questionRepondue.Juste = juste;


            IEnumerable<XElement> series = doc.Elements("serie");
            var serie = (from s in series
                             where s.LastAttribute.Value == matiere.ToString()
                             where s.Attribute("completed").Value == "false"
                             select s).FirstOrDefault();
            if (serie.Attribute("started").Value == "false")
                serie.SetAttributeValue("started", "true");

            int currentScore = int.Parse(serie.Attribute("score").Value);


            var question = (from q in serie.Elements("question")
                            where q.Attribute("done").Value == "false"
                            select q).FirstOrDefault();

            question.SetAttributeValue("done", "true");
            question.SetAttributeValue("right", juste.ToString());
            serie.SetAttributeValue("score", juste ? ++currentScore : currentScore);
            doc.WriteTo(XmlWriter.Create(new StringBuilder("questions.xml")));

            return currentScore;
        }

        public void serieFinished(int matiere)
        {
            currentSerie.Terminee = true;

            foreach(Serie serie in this.series)
            {
                Debug.Write(serie);
            }
            IEnumerable<XElement> series = doc.Elements("serie");

            var finishedSerie = (from s in series
                         where s.LastAttribute.Value == matiere.ToString()
                         where s.Attribute("completed").Value == "false"
                         select s).FirstOrDefault();

            var score = finishedSerie.Attribute("score").Value;
            finishedSerie.SetAttributeValue("completed", "true");

            doc.WriteTo(XmlWriter.Create(new StringBuilder("questions.xml")));
        }


    }
}
