﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Q_CM2
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            Model coucou = Model.getInstance();
        }

        private void btn_francais_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Resources["matiere"] = 0;
            Frame.Navigate(typeof(PageQCM));
        }

        private void btn_anglais_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Resources["matiere"] = 1;
            Frame.Navigate(typeof(PageQCM));
        }
        
        private void  btn_maths_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Resources["matiere"] = 2;
            Frame.Navigate(typeof(PageQCM));
        }

        private void btn_sciences_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Resources["matiere"] = 3;
            Frame.Navigate(typeof(PageQCM));
        }

        private void btn_histoire_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Resources["matiere"] = 4;
            Frame.Navigate(typeof(PageQCM));
        }

        private void btn_geo_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Resources["matiere"] = 5;
            Frame.Navigate(typeof(PageQCM));
        }
    }
}
