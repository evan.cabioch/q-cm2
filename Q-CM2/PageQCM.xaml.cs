﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

namespace Q_CM2
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class PageQCM : Page
    {
        Question question;
        Serie serie;
        int matiere;
        Button[] boutons = new Button[4];
        int score;

        public PageQCM()
        {
            Frame rootFrame = Window.Current.Content as Frame;
            this.InitializeComponent();
            SystemNavigationManager.GetForCurrentView().BackRequested += OnBackRequested;
            rootFrame.Navigated += OnRetour;




            boutons[0] = btnReponse1;
            boutons[1] = btnReponse2;
            boutons[2] = btnReponse3;
            boutons[3] = btnReponse4;


            matiere = (int)Application.Current.Resources["matiere"];

            serie = Model.getInstance().getFirstSerie(matiere);
            Debug.Write(serie);
            if (serie == null)
            {
                this.noMoreSerie();
            }

            else
            {
                question = serie.getFirstUnansweredQuestion();

                txtEnonce.DataContext = question;



                for (int i = 0; i < boutons.Length; i++)
                {
                    boutons[i].DataContext = question.getReponse(i);
                }
            }
        }

        private void OnBackRequested(object sender, BackRequestedEventArgs e)
        {

            Frame rootFrame = Window.Current.Content as Frame;


            if (rootFrame.CanGoBack)
            {
                e.Handled = true;
                rootFrame.GoBack();
            }

        }

        void OnRetour(Object sender, NavigationEventArgs e)
        {
            if (((Frame)sender).CanGoBack)
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
            }
            else
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
            }
        }

        private async void noMoreSerie()
        {
            ContentDialog dialog = new ContentDialog
            {
                Title = "Erreur",
                Content = "Il n'y a plus de série disponible pour cette matière.",
                PrimaryButtonText = "OK"
            };
            ContentDialogResult result = await dialog.ShowAsync();

            if(result == ContentDialogResult.Primary)
            {
                Frame rootFrame = Window.Current.Content as Frame;
                if (rootFrame.CanGoBack)
                {
                    rootFrame.GoBack();
                }
            }
        }

        public void nextQuestion()
        {
            Question nextQuestion = serie.getFirstUnansweredQuestion();

            question.updateQuestion(nextQuestion);
        }

        private void btnReponse1_Click(object sender, RoutedEventArgs e)
        {
            onButtonClicked(0);
        }

        private void btnReponse2_Click(object sender, RoutedEventArgs e)
        {
            onButtonClicked(1);
        }

        private void btnReponse3_Click(object sender, RoutedEventArgs e)
        {
            onButtonClicked(2);
        }

        private void btnReponse4_Click(object sender, RoutedEventArgs e)
        {
            onButtonClicked(3);
        }

        private async void onButtonClicked(int index)
        {
            
            bool correctAnswer = question.IndexBonneReponse == index;
            this.score = Model.getInstance().questionAnswered(serie.Matiere, correctAnswer);
            colorButton(correctAnswer, index);

            ContentDialog dialog = new ContentDialog
            {
                Title = string.Format("Score : {0} / 5", score),
                Content = (correctAnswer ? "Bravo, tu as bien répondu ! \n" : "Dommage, tu n'as trouvé la bonne réponse\n") + question.Explication,
                PrimaryButtonText = (serie.getFirstUnansweredQuestion() != null ? "Question suivante" : "Commencer une autre série")
            };

            ContentDialogResult result = await dialog.ShowAsync();

            if(result == ContentDialogResult.Primary)
            {
                if(serie.getFirstUnansweredQuestion() != null)
                {
                    this.nextQuestion();
                    
                    foreach(Button bouton in boutons)
                    {
                        bouton.ClearValue(Button.BackgroundProperty);
                    }

                }
                else
                {
                    Model.getInstance().serieFinished(serie.Matiere);

                    Frame rootFrame = Window.Current.Content as Frame;
                    if (rootFrame.CanGoBack)
                    {
                        rootFrame.GoBack();
                    }
                }
            }

    }

        private void colorButton(bool value, int index)
        {
            Button buttonClicked;
            switch (index)
            {
                case 0:
                    buttonClicked = btnReponse1;
                    break;
                case 1:
                    buttonClicked = btnReponse2;                   
                    break;
                case 2:
                    buttonClicked = btnReponse3;
                    break;
                default:
                    buttonClicked = btnReponse4;
                    break;
            }
            buttonClicked.Background = new SolidColorBrush(value ? Color.FromArgb(255, 0, 255, 0) : Color.FromArgb(255, 255, 0, 0));
        }
    }
}
