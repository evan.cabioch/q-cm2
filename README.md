# Description

## A propos de Q-CM2

Q-CM2 est une application Windows universelle destinée aux élève de CM2. Cette application rassemble des séries de QCM dans différentes matières scolaires (français, mathématiques, histoire, …).

## Principes de l’application

-	L’utilisateur peut choisir la matière sur laquelle il veut travailler depuis l’écran principal.
-	Une fois sur la page du QCM, l’utilisateur se voit proposer une question, avec plusieurs réponses (entre 2 et 4). Parmi elles une seule est correcte.
-	L’utilisateur doit répondre en moins de 30 secondes.
-	Après la réponse donnée, l’utilisateur voit s’il a bien ou mal répondu. Chaque réponse est associée à une brève explication. 
-	L’utilisateur peut à tout moment retourner à l’écran d’accueil, et sa progression dans la série est sauvegardée.
-	Une série est composée de 5 questions, à chaque réponse correcte, il marque un point.
-	L’utilisateur a aussi la possibilité de créer ses propres séries.
-	Les séries déjà complétées ne seront plus proposées à l’utilisateur.


## Stockage des QCM

Les séries, avec les questions et les réponses associées, sont stockées dans un fichier XML.
Le fichier sera composé de plusieurs séries. A l’intérieur d’une série se trouvent plusieurs questions.
Chaque question possède plusieurs réponses (celles proposées à l’utilisateur), et une brève explication.
Exemple de xml : 
```
<?xml version="1.0"?>
<serie matiere="0" completed="false" started="false">
	<question rigth="false" done="false">
		<enonce>2 + 2 = ?</enonce>
		<reponse good-answer="true">4</reponse>
		<reponse good-answer="false">3</reponse>
		<explication>En effet, lorsquekzjfksdlfjsdfkjh</explication>
	</question>
</serie>
```


# Conception

## Page d’accueil

	La page d’accueil est constituée de 6 boutons, permettant d’accéder au QCM des 6 matières (Français, Anglais, Mathématiques, Sciences, Histoire, Géographie). L’appui sur un des boutons amène directement sur une nouvelle série de QCM, sauf si une série de cette matière est déjà en cours.

## Page du QCM

Lors du passage à l’écran du QCM, l’application récupère la première série non complétée par l’utilisateur, et affiche la première question non effectuée de la série avec ses quatre réponses.
La page se divise en deux :
-	La partie « haute » de la page est consacrée à l’intitulé de la question.
-	La partie basse est composée de boutons, avec comme intitulés les réponses possibles.
-	En dessous de ces boutons se trouve une barre représentant le temps restant.
Lors de la soumission d’une réponse, une Dialog se lance, avec à l’intérieur :
-	Un intitulé « Vrai » ou « Faux »
-	La bonne réponse.
-	Le score sur la série en cours.
-	Une brève explication sur la question posée.
-	Un bouton
o	« Suivant » pour passer à la question suivante.
o	« Retour à l’Accueil » si la série est terminée.
Un bouton « Retour » permet de retourner à l’écran d’accueil, en mettant « en pause » la série. 

# Authors

Evan Cabioch et Guillaume Monrolin
